import os
import importlib

from flask import Flask, request, jsonify
from runner import Runner

app = Flask(__name__)
r = None

AVAILABLE_FORMATS = ["python", "c", "cpp"]


@app.route("/init", methods=["POST"])
def init():
    """
    Initialises the player

    Expects a JSON of the following format -
    {
        "player_id": 69,
        "format": "python",
        "file_name": "69.py"
    }
    """
    data = request.json

    if data["format"] not in AVAILABLE_FORMATS:
        return jsonify({
            "msg": "Invalid format"
        }), 400

    if data["format"] == "c":
        # TODO: Compile here
        pass
    elif data["format"] == "cpp":
        # TODO: Compile here
        pass
    elif data["format"] == "python":
        try:
            solution = importlib.import_module(
                f"submission.{data['player_id']}.{data['player_id']}"
            )
        except Exception as e:
            return jsonify({
                "msg": "Error while importing solution",
                "stacktrace": str(e)
            }), 400
    else:
        return jsonify({
            "msg": "Invalid format"
        }), 400

    format = None
    if data["format"] == "c" or data["format"] == "cpp":
        solution = f"submission/{data['player_id']/{data['file_name']}}"
        os.chmod(solution, 0o755)
        format = "executable"
    else:
        format = "python"

    global r
    r = Runner(format, solution)

    return jsonify({
        "msg": "Player initialized succesfully."
    })


@app.route("/", methods=["POST"])
def handle():
    """
    Pass state to function and return
    """
    state = request.json["state"]
    action = r.run(state)

    return jsonify({"action": action})


@app.route("/quit", methods=["POST"])
def quit():
    """
    Closes everything and shuts down
    """
    request.environ.get("werkzeug.server.shutdown")()
    return "Shutting down..."


app.run("0.0.0.0", 5000)
