"""
Sample solution

The problem given is:
    Given state s (int), return the next integer
"""


def solution(state):
    """
    Returns the action to be taken
    """
    return state["pending_edges"][0]
